input:
	myInput.txt -> source opcode table
	<asm File> -> user input as a command line argument

How to Run:
	
	extract source code form tar file.
	cd to /path/to/assembler
	make
	run :
		./exe <asm file>
output:
	generation of :
		symbol table
		literal table
		source opCode table

valiadtions Handled:
	
	1. repeatations of symbols

remaining work:
	
	1. error table
	2. register table
	3. Final opCode generation.


	
